define([
    'backbone',
    'spinalApp/SpinalApp.Utils',
    'underscore'
], function(Backbone, utils){ //, _){
    "use strict";

    Backbone.SpinalApp = (Backbone.SpinalApp)?Backbone.SpinalApp:{};

    Backbone.SpinalApp.Layout = Backbone.View.extend({
        typename: "SpinalApp.Layout",
        initialize: function(options){
            console.log(options);

            this.regionContents = [];
            this.saveOptions(options);
            if(this.regions === undefined){
                console.log("no regions in layout" , this.name, this);
                throw("must define regions in a layout" + this.options.layoutName) ;
            }
        },
        render: function(){
            //console.log("renmde", this.typename, this.modulename);

            if(this.template){ // TODO: a better way ?
                this.$el.append(this.template());
            } else {
                console.log("dont need to render");

            }
        },
        show: function(region, displayObject){
            if(this.regionContents.hasOwnProperty(region)){
                if(this.regionContents[region].hasOwnProperty("removeAll")){
                    this.regionContents[region].removeAll();
                } else  if(this.regionContents[region].hasOwnProperty("remove")){
                    this.regionContents[region].remove();
                }
            }
            this.regionContents[region] = displayObject;

            this.$(this.regions[region]).html("");

            if(typeof displayObject === "function"){
                this.$(this.regions[region]).append(displayObject());

            } else {
                //console.log(displayObject);
                //console.log(displayObject.modulename);
                //console.log(displayObject.typename);
                //console.log(displayObject.el);

                displayObject.render();
                this.$(this.regions[region]).append(displayObject.el);
            }

            return displayObject;
        }
    });

    Backbone.SpinalApp.Layout.prototype.saveOptions = utils.saveOptions;

    return Backbone.SpinalApp.Layout;
});