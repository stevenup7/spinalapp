define(['backbone'], function(Backbone){
    "use strict";

    Backbone.SpinalApp = (Backbone.SpinalApp)?Backbone.SpinalApp:{};

    Backbone.SpinalApp.View = Backbone.View.extend({
        typename: "SpinalApp.View",
        initialize: function(){
            if(this.options.template){
                this.template = this.options.template;
            } else {
                throw("SpinalAppNoTemplate");
            }
        },
        render: function(){
            this.$el.append(this.template());
        }
    });

    return Backbone.SpinalApp.View;
});