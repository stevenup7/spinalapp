define(['backbone', 'underscore'], function(Backbone, _){

    Backbone.SpinalApp = (Backbone.SpinalApp)?Backbone.SpinalApp:{};

    Backbone.SpinalApp.Utils = {};

    Backbone.SpinalApp.Utils.saveOptions = function(options){
        var saveableOptions = ["name", "template", "regions", "layout", "view", "el", "$el"];
        this.options = {};
        _.each(options, function(v, k){
            if(saveableOptions.indexOf(k) === -1){
                this.options[k] = v;
            } else {
                this[k] = v;
            }
        }, this);
    };

    Backbone.SpinalApp.Utils.checkOptions = function(obj){
        _.each(obj, function(v, k){
            console.log(k,v);
        });
    };

    return Backbone.SpinalApp.Utils;
});

