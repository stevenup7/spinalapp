define([
    'backbone',
    'underscore',
    'spinalApp/SpinalApp.Utils'
], function(Backbone, _, utils){
    "use strict";

    Backbone.SpinalApp = (Backbone.SpinalApp)?Backbone.SpinalApp:{};

    Backbone.SpinalApp.App = function (options) {
        this.typename = "SpinalApp.App";
        this.router = new Backbone.Router(); // base backbone router

        this.saveOptions(options);

        this.modules = [];
        this.currentModule = null;
        this.initialize();
    };

    Backbone.SpinalApp.App.prototype.initialize = function(){
        // stub
    };

    Backbone.SpinalApp.App.prototype.start = function(startOpts){
        try{
            Backbone.history.start();
        }catch(x){ }

        _.each(this.modules, function(module){
            if(module.autostart){
                module.start();
            }
        }, this);

        if(this.hasOwnProperty("layout")){
            // console.log("gonna render app");
            this.layout.render();
        }

        this.onStart(startOpts); // pass in the params
        return this;
    };

    Backbone.SpinalApp.App.prototype.onStart = function(){
        // stub
    };

    Backbone.SpinalApp.App.prototype.stop = function (){
        _.each(this.modules, function(module){
            module.stop();
        }, this);
        this.onStop();
    };

    Backbone.SpinalApp.App.prototype.onStop = function (){
        // todo stopModules
    };

    Backbone.SpinalApp.App.prototype.addModule = function(newModule){
        this.modules.push(newModule);
        newModule.parent = this;
        newModule.added();
        return newModule;
    };

    Backbone.SpinalApp.App.prototype.removeModule = function(oldModule){
        this.modules = _.reject(this.modules, function(m){
            return m === oldModule;
        });
    };

    Backbone.SpinalApp.App.prototype.setLayout = function(layout){
        //console.log("setting layout");

        this.layout = layout;
    };


    Backbone.SpinalApp.App.prototype.setModule = function(module){
        if(module !== this.currentModule){
            if(this.currentModule !== null){
                this.currentModule.stop();
            }
            this.currentModule = module;
            this.currentModule.start();
        }
    };

    Backbone.SpinalApp.App.prototype.registerRoute = function(routeDef, module, callback){
        //console.log("adding route -- ", routeDef);

        var app = this;
        this.router.route(routeDef, module, function(){
            //console.log("args", arguments);

            if(module.parent === app){
                app.setModule(module);
            }
            module[callback].apply(module, arguments);
        });
    };


    Backbone.SpinalApp.App.prototype.saveOptions = utils.saveOptions;

    Backbone.SpinalApp.App.extend = Backbone.Model.extend;
    return Backbone.SpinalApp.App;

});