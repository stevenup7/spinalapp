define(['backbone', 'underscore'], function(Backbone, _){
    "use strict";

    // create the spinal App property of backbone
    Backbone.SpinalApp = (Backbone.SpinalApp)?Backbone.SpinalApp:{};

    Backbone.SpinalApp.Module = function (options) {
        this.typename = "SpinalApp.Module";
        this.options = options || {};
        this.views = [];
        this.modules = [];
        this.regions = [];

        if(this.options.layout){
            this.layout = this.options.layout;
        }
        if(this.options.modulename){
            this.modulename = this.options.modulename;
        }

        this.initialize();

        if(this.layout === undefined){
            throw("you gotta declare a layout");
        }
        if(this.modulename === undefined){
            throw("you gotta name your module");
        }
    };


    Backbone.SpinalApp.Module.prototype.added = function(){
        // called after Added
        this.addRoutes();
    };

    Backbone.SpinalApp.Module.prototype.registerRoute = function(route, module, callback){
        // pass registerRoute up from callbacks
        this.parent.registerRoute(route, module, callback);
    };

    Backbone.SpinalApp.Module.prototype.addRoutes = function(){
        if(this.routes){
            _.each(this.routes, function(callback, route){
                this.parent.registerRoute(route, this, callback);
            }, this);
        }
    };

    Backbone.SpinalApp.Module.prototype.initialize = function(){
        // stub
    };

    Backbone.SpinalApp.Module.prototype.start = function(startOpts){
        this.onStart(startOpts); // pass in the params
        _.each(this.modules, function(module){
            module.start();
        });
        return this;
    };

    Backbone.SpinalApp.Module.prototype.onStart = function(){
        // stub
    };

    Backbone.SpinalApp.Module.prototype.stop = function (){
        this.onStop();
        return this;
    };

    Backbone.SpinalApp.Module.prototype.onStop = function(){
        // stub
    };

    Backbone.SpinalApp.Module.prototype.render = function (){
        this.layout.render();

    };

    Backbone.SpinalApp.Module.prototype.addView = function(newView){
        this.views.push(newView);
        newView.parent = this;
    };


    Backbone.SpinalApp.Module.prototype.removeView = function(oldView){
        this.views = _.reject(this.views, function(view){return view === oldView;});
    };

    Backbone.SpinalApp.Module.prototype.addModule = function(newModule){
        this.modules.push(newModule);
        newModule.parent = this;
    };

    Backbone.SpinalApp.Module.prototype.removeModule = function(oldModule){
        this.modules = _.reject(this.modules, function(m){
            return m === oldModule;
        });
    };



    // borrow the extend prototype from the Backbone Model - there must be a better way
    Backbone.SpinalApp.Module.extend = Backbone.Model.extend;
    return Backbone.SpinalApp.Module;

});