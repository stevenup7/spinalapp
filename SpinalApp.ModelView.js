define(['backbone', 'spinalApp/SpinalApp.View'], function(Backbone){
    "use strict";


    Backbone.SpinalApp.ModelView = Backbone.SpinalApp.View.extend({
        typename: "SpinalApp.ModelView",
        initialize: function(){
            if(this.options.template){
                this.template = this.options.template;
            }
        }

    });

    return Backbone.SpinalApp.ModelView;
});